module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "md-demo-s3-bucket"
  acl    = "private"

  versioning = {
    enabled = true
  }

}
