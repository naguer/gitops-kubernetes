In here, you will find six files used to provision a VPC, security groups and an EKS cluster. The final product should be similar to this:

* vpc.tf provisions a VPC, subnets and availability zones using the AWS VPC Module. A new VPC is created for this tutorial so it doesn't impact your existing cloud environment and resources.

* security-groups.tf provisions the security groups used by the EKS cluster.

* eks-cluster.tf provisions all the resources (AutoScaling Groups, etc...) required to set up an EKS cluster using the AWS EKS Module.

* outputs.tf defines the output configuration.

* versions.tf sets the Terraform version to at least 0.14. It also sets versions for the providers used in this sample.
