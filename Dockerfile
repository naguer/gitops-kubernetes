FROM python:3.8-alpine
MAINTAINER Nahuel Hernandez "contact@nahuelhernandez.com"

# I copy just the requirements.txt first to leverage Docker cache
COPY src/requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt

# Copy App files
COPY src /app

ENV FLASK_APP run.py

# Add the port metadata information
EXPOSE 5000

ENTRYPOINT ["gunicorn"]
CMD ["--config", "gunicorn-cfg.py", "run:app"]
