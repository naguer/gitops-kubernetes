# Kubernetes Pipelines using Terraform

This demo shows how to create a Kubernetes environment on AWS using Terraform and how to configure a CICD using GitlabCI and ArgoCD/HELM. The application mounts the Kubernetes enviroments values, and also the application scale automatically increasing or decreasing the number of Pods in response to the workload's CPU.



## Objectives

* Containerize Flask App "Hello World"
* Create K8S Cluster (EKS) using Terraform
* Deploy Flask App on EKS
* Create an Object Storage (S3 Bucket) using Terraform
* Configure a CICD to automate building and deploying the Flask App on K8S



## Requirements

* AWS Account
* Docker Hub Account
* Kubectl => 1.22
* AWS Cli => 2.1
* Terraform => 1.0.0
* Helm => 3.5

## Directory Structure

```
├── Dockerfile
├── helm-charts
│   ├── Chart.yaml
│   ├── templates
│   ├── values-dev.yaml
│   └── values.yaml
├── images
│   ├── app.png
│   └── argo.png
├── k8s-files
│   ├── argoproj.yml
├── README.md
├── src
│   ├── gunicorn-cfg.py
│   ├── requirements.txt
│   ├── run.py
│   ├── static
│   └── templates
└── terraform
    ├── eks-cluster.tf
    ├── kubeconfig_demo-eks-3DeLAQZ9
    ├── kubernetes-dashboard-admin.rbac.yaml
    ├── kubernetes.tf
    ├── outputs.tf
    ├── README.md
    ├── s3.tf
    ├── security-groups.tf
    ├── versions.tf
    └── vpc.tf
```
* The `src` directory is for the application code
* The `k8s-files` is for the K8S resources without helm
* The `terraform` directory is to create the VPC, S3 and EKS resource
* The `helm-charts` if for the K8S resources, and application configuration



## Steps

First, we need to deploy the repository:

```
> git clone git@gitlab.com:naguer/gitops-kubernetes.git
```



### Create the VPC, S3 Bucket and EKS resources on AWS using Terraform

Configure AWS Credentials

```
> aws configure
```

Terraform init to initialize the environment and apply to create the env

```
> cd terraform 
> terraform init
> terraform apply 
Apply complete! Resources: 50 added, 0 changed, 13 destroyed.

Outputs:
cluster_endpoint = "https://C801E02478D6312BF12B860D1E35E14E.gr7.us-east-2.eks.amazonaws.com"
cluster_id = "demo-eks-3DeLAQZ9"
cluster_name = "demo-eks-3DeLAQZ9"...
```

Note: This job takes approximately 20 minutes.



Now we can connect to the cluster using the `cluster_id` and the `region` value and we can view the namespaces

```
> aws eks update-kubeconfig --name demo-eks-3DeLAQZ9 --region us-east-2
> kubectl get ns                                                                                                                
NAME              STATUS   AGE
default           Active   16h
kube-node-lease   Active   16h
kube-public       Active   16h
kube-system       Active   16h
```



I want to configure a Metric server because i will need it to configure HPA

```
> kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
```



Testing

```
> kubectl top nodes                                                                                                               
NAME                                       CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
ip-10-0-1-65.us-east-2.compute.internal    46m          4%     602Mi           39%
ip-10-0-2-163.us-east-2.compute.internal   90m          4%     785Mi           23%
ip-10-0-3-190.us-east-2.compute.internal   41m          4%     633Mi           42%
```



### Continuos Integration

For the continuous integration, i will use Gitlab CI and upload the container image to Docker Hub.
The pipeline configuration is on the `.gitlab-ci.yml` file on the repo. So, We only need to configure the Docker Hub environment variables. 

Settings -> CICD -> Variables

* CI_REGISTRY: docker.io
* CI_REGISTRY_IMAGE: index.docker.io/naguer/flask-demo
* CI_REGISTRY_PASSWORD: "my private password"
* CI_REGISTRY_USER: naguer

The pipeline will only run if we make a PR to the master branch, if only when the Dockerfile exists.

```
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
      exists:
        - Dockerfile
```



### Continous Deployment 

A good solution for the CD part is Argocd + Helm. Why Argo CD? Application definitions, configurations, and environments should be declarative and version controlled. Application deployment and lifecycle management should be automated, auditable, and easy to understand.

```
> helm repo add argo https://argoproj.github.io/argo-helm
> kubectl create namespace argocd
> helm install argocd argo/argo-cd -n argocd
```

Expose Argo Website to the world

```
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

Get the initial password

```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

Login to Argo, username is `admin`

```
> kubectl get svc -n argocd | grep LoadBalancer
> argocd login ad7ee1c6e0d994e9abb30edb52c49b19-2029725375.us-east-2.elb.amazonaws.com
'admin:login' logged in successfully
Context 'ad7ee1c6e0d994e9abb30edb52c49b19-2029725375.us-east-2.elb.amazonaws.com' updated
```

Update the password

```
> argocd account update-password
Password updated
Context 'ad7ee1c6e0d994e9abb30edb52c49b19-2029725375.us-east-2.elb.amazonaws.com' updated
```



Configure Argocd permissions to access our repo

```
> argocd repo add  git@gitlab.com:naguer/gitops-kubernetes.git --ssh-private-key-path ~/.ssh/id_rsa
Repository 'git@gitlab.com:naguer/gitops-kubernetes.git' added
```



Create the application.

```
argocd app create playhouse-course-api \
--repo git@gitlab.com:digitalhouse-team/do-sq/helm/pg.git \
--path playhouse-course-api-qa \
--dest-server https://kubernetes.default.svc \
--dest-namespace playground-dev \
--sync-policy automated \
--auto-prune \
--self-heal
```


We can access to the webargo page on the loadbalancer url: ad7ee1c6e0d994e9abb30edb52c49b19-2029725375.us-east-2.elb.amazonaws.com

And check the application.

![](images/argo.png)



If we want to change any important value of the app, we can do it with the `helm-charts/values-dev.yaml` file

```replicaCount: 3
image:
  repository: naguer/flask-demo
  tag: latest
service:
  type: LoadBalancer
  port: 80
  targetPort: 5000
hpa:
  minReplicas: 3
  maxReplicas: 5
  targetCPUUtilizationPercentage: 70
resources:
  limits:
    cpu: 100m
    memory: 100Mi
  requests:
    cpu: 50m
    memory: 50Mi
```

Any time we change something of the app configuration such the replicas, limits, etc on the Github repository. Argo will update the changes on Kubernetes. 



### Last Check

Now we can access our application using the Service endpoint.  http://a4327f1bcbbdb4f65bb66890f3265b24-1751052529.us-east-2.elb.amazonaws.com

```
> kubectl get svc flask-demo
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP                                                               PORT(S)        AGE
flask-demo   LoadBalancer   172.20.222.246   a4327f1bcbbdb4f65bb66890f3265b24-1751052529.us-east-2.elb.amazonaws.com   80:31973/TCP   68m
```

![](images/app.png)

Note: The application shows some environment variables and a secret value created on K8S. If we refresh the webpage the env values (Node, podname, ip) will change bacause another POD respond to us. 



## EXTRA HPA

The application have 3 replicas and could scale to 5 if the cpu is > %30

```
> kubectl get pods,hpa                                                                                                                        ✔  demo-eks-3DeLAQZ9 ⎈
NAME                              READY   STATUS    RESTARTS   AGE
pod/flask-demo-74b8788c59-85tvk   1/1     Running   0          85m
pod/flask-demo-74b8788c59-fr42l   1/1     Running   0          85m
pod/flask-demo-74b8788c59-whps2   1/1     Running   0          85m

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/flask-demo   Deployment/flask-demo   2%/30%    3         5         3          113m
```


Making a stress test

```
> kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://a4327f1bcbbdb4f65bb66890f3265b24-1751052529.us-east-2.elb.amazonaws.com; done"
```


After the stress test:

```
> kubectl get pods,hpa                                                                                       
NAME                              READY   STATUS    RESTARTS   AGE
pod/flask-demo-74b8788c59-85tvk   1/1     Running   0          85m
pod/flask-demo-74b8788c59-bj74w   1/1     Running   0          24s
pod/flask-demo-74b8788c59-fr42l   1/1     Running   0          85m
pod/flask-demo-74b8788c59-k7trr   1/1     Running   0          24s
pod/flask-demo-74b8788c59-whps2   1/1     Running   0          85m
pod/load-generator               1/1     Running   0          57s

NAME                                             REFERENCE               TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/flask-demo   Deployment/flask-demo   52%/30%   3         5         5          113m
```

The application scaled automatically to 5 replicas. 


## Bonus Section

* What enhancements of the current solution would you propose if given more time?

  * Create another repository and separate the Kubernetes/Helm files from the app code
  * Improve the documentation
  * Improve the pipeline with tests and linters
  * Don't use `latest` on the images
  * Add architecture diagram
  * Add Terraform variables files
  * Terraform remote state on a S3 Bucket, and Dynamodb table for the locking part
  * Add Kubernetes probes
  * Use Kubernetes Configmaps instead to env variables 
  * Add another tool for secrets such Hashicorp Vault, AWS Secret Manager, etc (K8S secret is not really secure)
  
* What would be your ideal setup?
  
  I use Argo/Helm/Terrafom on my daily, this is my ideal setup. Maybe add Terraform for multienv (Terragrunt/Scripting) with a pipeline or add Rancher to manage more than one K8S Cluster. For the Observability part i like Datadog for metrics and logging or EFK (logging) and Promethues/Grafana (Metrics)

* How would you suggest managing multiple environments?

  Creating 2 more cluster on K8S (dev, qa, prod), and also adding the multienv part on Terraform. The applications needs to follow the https://12factor.net/ principles, if we want to promote the Docker images on the K8S Cluster. 

* How would you scale the Kubernetes setup?

  I added HPA and Cluster Autoscaler to this Setup.  

* How would you implement health checks?

  Use readiness, liveness, and startup probes on Kubernetes.


## Extras

* Added to the App envs OS values such `namespace` `podname` `pod_ip` and `node_name` 
* Changed the "hello world" app to one better that use the env variables and show the secret value
* Installed Kubernetes Metric Server
* Configured Autoscaling (HPA) on K8S (min replicas 3, max 5 on cpu > 30%) and Cluster Autoscaler



## References:

* https://github.com/kubernetes/kubernetes/issues/33664
* https://flask.palletsprojects.com/en/2.0.x/quickstart/#a-minimal-application
* https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-kubectl/
* https://docs.openshift.com/container-platform/4.1/nodes/pods/nodes-pods-secrets.html
* https://argo-cd.readthedocs.io/en/stable/
* https://helm.sh/docs/
